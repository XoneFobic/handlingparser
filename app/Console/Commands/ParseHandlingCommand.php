<?php

namespace App\Console\Commands;

use App\VehicleHandling;
use Illuminate\Console\Command;
use XmlParser;

class ParseHandlingCommand extends Command {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'xml:parse';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Parse "handling.meta" file.';

  private $blacklist = [
    'ALPHAZ1',
    'ANNIHL',
    'AVENGER',
    'BESRA',
    'BLIMP',
    'BLIMP2',
    'BUZZARD',
    'BUZZARD2',
    'CARGOBOB',
    'CARGOPLANE',
    'CUBAN800',
    'DINGHY',
    'DINGHY2',
    'DINGHY3',
    'DINGHY4',
    'DUSTER',
    'FROGGER',
    'FROGGER2',
    'HYDRA',
    'JET',
    'JETMAX',
    'LAZER',
    'LUXOR',
    'LUXOR2',
    'MAMMATUS',
    'MARQUIS',
    'MILJET',
    'NIMBUS',
    'SEAPLANE',
    'SEASHARK',
    'SEASHARK2',
    'SEASHARK3',
    'SHAMAL',
    'SKYLIFT',
    'SPEEDER',
    'SPEEDER2',
    'SQUALO',
    'STRIKEFORCE',
    'STUNT',
    'SUBMERSIBLE',
    'SUBMERSIBLE2',
    'SUNTRAP',
    'SVOLITO',
    'SWIFT',
    'SWIFT2',
    'TITAN',
    'TORO',
    'TORO2',
    'TROPIC',
    'TROPIC2',
    'TUG',
    'VELUM',
    'VESTRA',
    'VOLATUS',
  ];

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct () {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle () {
    $xml = XmlParser::load('./resources/assets/handling.meta');

    VehicleHandling::truncate();

    foreach ($xml->getContent()->HandlingData->Item as $tune) {
      $tune = json_decode(json_encode($tune));

      if (!in_array(strtoupper($tune->handlingName), $this->blacklist)) {
        $handling = new VehicleHandling();
        $handling->id = $tune->handlingName;
        $handling->fMass = $tune->fMass->{'@attributes'}->value;
        $handling->fInitialDragCoeff = $tune->fInitialDragCoeff->{'@attributes'}->value;
        $handling->fPercentSubmerged = $tune->fPercentSubmerged->{'@attributes'}->value;
        $handling->fDriveBiasFront = $tune->fDriveBiasFront->{'@attributes'}->value;
        $handling->nInitialDriveGears = $tune->nInitialDriveGears->{'@attributes'}->value;
        $handling->fInitialDriveForce = $tune->fInitialDriveForce->{'@attributes'}->value;
        $handling->fDriveInertia = $tune->fDriveInertia->{'@attributes'}->value;
        $handling->fClutchChangeRateScaleUpShift = $tune->fClutchChangeRateScaleUpShift->{'@attributes'}->value;
        $handling->fClutchChangeRateScaleDownShift = $tune->fClutchChangeRateScaleDownShift->{'@attributes'}->value;
        $handling->fInitialDriveMaxFlatVel = $tune->fInitialDriveMaxFlatVel->{'@attributes'}->value;
        $handling->fBrakeForce = $tune->fBrakeForce->{'@attributes'}->value;
        $handling->fBrakeBiasFront = $tune->fBrakeBiasFront->{'@attributes'}->value;
        $handling->fHandBrakeForce = $tune->fHandBrakeForce->{'@attributes'}->value;
        $handling->fSteeringLock = $tune->fSteeringLock->{'@attributes'}->value;
        $handling->fTractionCurveMax = $tune->fTractionCurveMax->{'@attributes'}->value;
        $handling->fTractionCurveMin = $tune->fTractionCurveMin->{'@attributes'}->value;
        $handling->fTractionCurveLateral = $tune->fTractionCurveLateral->{'@attributes'}->value;
        $handling->fTractionSpringDeltaMax = $tune->fTractionSpringDeltaMax->{'@attributes'}->value;
        $handling->fLowSpeedTractionLossMult = $tune->fLowSpeedTractionLossMult->{'@attributes'}->value;
        $handling->fCamberStiffnesss = $tune->fCamberStiffnesss->{'@attributes'}->value;
        $handling->fTractionBiasFront = $tune->fTractionBiasFront->{'@attributes'}->value;
        $handling->fTractionLossMult = $tune->fTractionLossMult->{'@attributes'}->value;
        $handling->fSuspensionForce = $tune->fSuspensionForce->{'@attributes'}->value;
        $handling->fSuspensionCompDamp = $tune->fSuspensionCompDamp->{'@attributes'}->value;
        $handling->fSuspensionReboundDamp = $tune->fSuspensionReboundDamp->{'@attributes'}->value;
        $handling->fSuspensionUpperLimit = $tune->fSuspensionUpperLimit->{'@attributes'}->value;
        $handling->fSuspensionLowerLimit = $tune->fSuspensionLowerLimit->{'@attributes'}->value;
        $handling->fSuspensionRaise = $tune->fSuspensionRaise->{'@attributes'}->value;
        $handling->fSuspensionBiasFront = $tune->fSuspensionBiasFront->{'@attributes'}->value;
        $handling->fAntiRollBarForce = $tune->fAntiRollBarForce->{'@attributes'}->value;
        $handling->fAntiRollBarBiasFront = $tune->fAntiRollBarBiasFront->{'@attributes'}->value;
        $handling->fRollCentreHeightFront = $tune->fRollCentreHeightFront->{'@attributes'}->value;
        $handling->fRollCentreHeightRear = $tune->fRollCentreHeightRear->{'@attributes'}->value;
        $handling->fCollisionDamageMult = 5;
        $handling->fWeaponDamageMult = 5;
        $handling->fDeformationDamageMult = 5;
        $handling->fEngineDamageMult = 5;
        $handling->fPetrolTankVolume = $tune->fPetrolTankVolume->{'@attributes'}->value;
        $handling->fOilVolume = $tune->fOilVolume->{'@attributes'}->value;
        $handling->fSeatOffsetDistX = $tune->fSeatOffsetDistX->{'@attributes'}->value;
        $handling->fSeatOffsetDistY = $tune->fSeatOffsetDistY->{'@attributes'}->value;
        $handling->fSeatOffsetDistZ = $tune->fSeatOffsetDistZ->{'@attributes'}->value;
        $handling->nMonetaryValue = $tune->nMonetaryValue->{'@attributes'}->value;

        $handling->fDownforceModifier = property_exists($tune, 'fDownforceModifier')
          ? $tune->fDownforceModifier->{'@attributes'}->value
          : null;

        $handling->vecCentreOfMassOffset = json_encode([
          "x" => floatval($tune->vecCentreOfMassOffset->{'@attributes'}->x),
          "y" => floatval($tune->vecCentreOfMassOffset->{'@attributes'}->y),
          "z" => floatval($tune->vecCentreOfMassOffset->{'@attributes'}->z)
        ]);

        $handling->vecInertiaMultiplier = json_encode([
          "x" => floatval($tune->vecInertiaMultiplier->{'@attributes'}->x),
          "y" => floatval($tune->vecInertiaMultiplier->{'@attributes'}->y),
          "z" => floatval($tune->vecInertiaMultiplier->{'@attributes'}->z)
        ]);

        $handling->save();
      }
    }

    return true;
  }
}
