<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VehicleHandling
 *
 * @property int $id
 * @property float|null $fMass
 * @property float|null $fInitialDragCoeff
 * @property float|null $fDownforceModifier
 * @property float|null $fPercentSubmerged
 * @property mixed|null $vecCentreOfMassOffset
 * @property mixed|null $vecInertiaMultiplier
 * @property float|null $fDriveBiasFront
 * @property int|null $nInitialDriveGears
 * @property float|null $fInitialDriveForce
 * @property float|null $fDriveInertia
 * @property float|null $fClutchChangeRateScaleUpShift
 * @property float|null $fClutchChangeRateScaleDownShift
 * @property float|null $fInitialDriveMaxFlatVel
 * @property float|null $fBrakeForce
 * @property float|null $fBrakeBiasFront
 * @property float|null $fHandBrakeForce
 * @property float|null $fSteeringLock
 * @property float|null $fTractionCurveMax
 * @property float|null $fTractionCurveMin
 * @property float|null $fTractionCurveLateral
 * @property float|null $fTractionSpringDeltaMax
 * @property float|null $fLowSpeedTractionLossMult
 * @property float|null $fCamberStiffnesss
 * @property float|null $fTractionBiasFront
 * @property float|null $fTractionLossMult
 * @property float|null $fSuspensionForce
 * @property float|null $fSuspensionCompDamp
 * @property float|null $fSuspensionReboundDamp
 * @property float|null $fSuspensionUpperLimit
 * @property float|null $fSuspensionLowerLimit
 * @property float|null $fSuspensionRaise
 * @property float|null $fSuspensionBiasFront
 * @property float|null $fAntiRollBarForce
 * @property float|null $fAntiRollBarBiasFront
 * @property float|null $fRollCentreHeightFront
 * @property float|null $fRollCentreHeightRear
 * @property float|null $fCollisionDamageMult
 * @property float|null $fWeaponDamageMult
 * @property float|null $fDeformationDamageMult
 * @property float|null $fEngineDamageMult
 * @property float|null $fPetrolTankVolume
 * @property float|null $fOilVolume
 * @property float|null $fSeatOffsetDistX
 * @property float|null $fSeatOffsetDistY
 * @property float|null $fSeatOffsetDistZ
 * @property int|null $nMonetaryValue
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFAntiRollBarBiasFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFAntiRollBarForce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFBrakeBiasFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFBrakeForce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFCamberStiffnesss($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFClutchChangeRateScaleDownShift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFClutchChangeRateScaleUpShift($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFCollisionDamageMult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFDeformationDamageMult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFDownforceModifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFDriveBiasFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFDriveInertia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFEngineDamageMult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFHandBrakeForce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFInitialDragCoeff($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFInitialDriveForce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFInitialDriveMaxFlatVel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFLowSpeedTractionLossMult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFMass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFOilVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFPercentSubmerged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFPetrolTankVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFRollCentreHeightFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFRollCentreHeightRear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSeatOffsetDistX($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSeatOffsetDistY($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSeatOffsetDistZ($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSteeringLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionBiasFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionCompDamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionForce($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionLowerLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionRaise($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionReboundDamp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFSuspensionUpperLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFTractionBiasFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFTractionCurveLateral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFTractionCurveMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFTractionCurveMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFTractionLossMult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFTractionSpringDeltaMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereFWeaponDamageMult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereNInitialDriveGears($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereNMonetaryValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereVecCentreOfMassOffset($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VehicleHandling whereVecInertiaMultiplier($value)
 * @mixin \Eloquent
 */
class VehicleHandling extends Model {
  public $timestamps = false;
  protected $table = 'vehicle_handling';
}
