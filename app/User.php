<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\User
 *
 * @property int $id
 * @property string $identifier
 * @property string|null $name
 * @property string|null $license
 * @property string|null $ip
 * @property int|null $permission_level
 * @property string|null $group
 * @property int|null $charid
 * @property int|null $last_known_id
 * @property string|null $last_changed
 * @property string|null $first_joined
 * @property string|null $watched
 * @property string|null $watched_date
 * @property int|null $vip
 * @property string|null $donator_until
 * @property string $mic_setup
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCharid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDonatorUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFirstJoined($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastChanged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastKnownId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLicense($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereMicSetup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePermissionLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVip($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWatched($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWatchedDate($value)
 * @mixin \Eloquent
 */
class User extends Model {
}
